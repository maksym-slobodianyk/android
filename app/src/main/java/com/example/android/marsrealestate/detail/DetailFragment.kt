package com.example.android.marsrealestate.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.android.marsrealestate.databinding.FragmentDetailBinding

class DetailFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        @Suppress("UNUSED_VARIABLE")
        val application = requireNotNull(activity).application
        val binding = FragmentDetailBinding.inflate(inflater)

        val arguments = DetailFragmentArgs.fromBundle(arguments!!)
        val viewModelFactory = DetailViewModelFactory(arguments.postId, application)
        val postDetailViewModel =
                ViewModelProviders.of(
                        this, viewModelFactory).get(DetailViewModel::class.java)
        binding.viewModel = postDetailViewModel
        val adapter = CommentsListAdapter()
        binding.commentsList.adapter = adapter

        postDetailViewModel.getComments().observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.submitList(it)
            }
        })

        binding.setLifecycleOwner(this)
        return binding.root
    }
}