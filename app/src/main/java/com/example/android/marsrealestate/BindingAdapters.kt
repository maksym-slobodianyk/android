package com.example.android.marsrealestate

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.android.marsrealestate.network.CommentProperty
import com.example.android.marsrealestate.network.PostProperty

@BindingAdapter("overviewTitle")
fun TextView.setTitle(item: PostProperty?) {
    item?.let { text = item.title }
}

@BindingAdapter("overviewBody")
fun TextView.setBody(item: PostProperty?) {
    item?.let { text = "${item.body.substring(0, 50)}..." }
}

@BindingAdapter("commentEmail")
fun TextView.setEmail(item: CommentProperty?) {
    item?.let { text = item.email }
}

@BindingAdapter("commentBody")
fun TextView.setBody(item: CommentProperty?) {
    item?.let { text = item.body }
}
