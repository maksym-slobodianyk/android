package com.example.android.marsrealestate.network

data class CommentProperty(
        val id: Long,
        val body: String,
        val email: String)