package com.example.android.marsrealestate.network

data class PostProperty(
        val id: Long,
        val userId: Long,
        val title: String,
        val body: String)
