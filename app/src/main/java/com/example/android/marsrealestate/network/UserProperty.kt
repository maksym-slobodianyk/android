package com.example.android.marsrealestate.network

data class UserProperty(
        val id: Long,
        val name: String,
        val website: String)
