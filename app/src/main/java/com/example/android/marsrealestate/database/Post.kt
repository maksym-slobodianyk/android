package com.example.android.marsrealestate.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "posts")
data class Post  (
        @PrimaryKey(autoGenerate = false)
        var id: Long = 0L,
        var userId: Long,
        val title: String,
        var body: String
)