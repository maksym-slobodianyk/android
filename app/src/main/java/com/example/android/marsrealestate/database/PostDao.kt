package com.example.android.marsrealestate.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface PostDao {
    @Insert
    fun insert(post: Post)

    @Query("SELECT * from posts WHERE id = :postId")
    fun get(postId: Long): Post?

}