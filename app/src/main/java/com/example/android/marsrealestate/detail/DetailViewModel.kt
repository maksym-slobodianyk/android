package com.example.android.marsrealestate.detail

import android.app.Application
import androidx.lifecycle.*
import com.example.android.marsrealestate.network.CommentProperty
import com.example.android.marsrealestate.network.PostApi
import com.example.android.marsrealestate.network.PostProperty
import com.example.android.marsrealestate.network.UserProperty
import com.example.android.marsrealestate.overview.OverviewViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class DetailViewModel(val postId: Long, app: Application) : AndroidViewModel(app) {

    enum class PostApiStatus { FETCHING, ERROR, DONE }

    private val viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val _status = MutableLiveData<OverviewViewModel.PostApiStatus>()
    val status: LiveData<OverviewViewModel.PostApiStatus>
        get() = _status

    private val _user = MediatorLiveData<UserProperty>()
    val user: LiveData<UserProperty>
        get() = _user

    private val _post = MediatorLiveData<PostProperty>()
    val post: LiveData<PostProperty>
        get() = _post

    private val _comments = MediatorLiveData<List<CommentProperty>>()
    fun getComments() = _comments

    init {
        getPostDetails();
    }

    private fun getPostDetails() {
        coroutineScope.launch {
            val getPostDeferred = PostApi.retrofitService.getPostById(postId)
            val getCommentsDeferred = PostApi.retrofitService.getPostCommnets(postId)
            try {
                _status.value = OverviewViewModel.PostApiStatus.FETCHING

                val receivedPost = getPostDeferred.await()
                _post.value = receivedPost

                val getUserDeferred = PostApi.retrofitService.getuserById(receivedPost.userId)
                _user.value = getUserDeferred.await()
                val comments = getCommentsDeferred.await()
                _comments.value = comments;
                _status.value = OverviewViewModel.PostApiStatus.DONE

            } catch (t: Throwable) {
                _status.value = OverviewViewModel.PostApiStatus.ERROR
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}
