package com.example.android.marsrealestate.overview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.network.PostApi
import com.example.android.marsrealestate.network.PostProperty
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class OverviewViewModel : ViewModel() {

    enum class PostApiStatus { FETCHING, ERROR, DONE }

    private val _properties = MutableLiveData<List<PostProperty>>()
    val properties: LiveData<List<PostProperty>>
        get() = _properties

    private val _status = MutableLiveData<PostApiStatus>()
    val status: LiveData<PostApiStatus>
        get() = _status

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)


    init {
        getMarsRealEstateProperties()
    }

    private fun getMarsRealEstateProperties() {
        coroutineScope.launch {
            var getPropertiesDeferred = PostApi.retrofitService.getProperties()
            try {
                _status.value = PostApiStatus.FETCHING
                var listResult = getPropertiesDeferred.await()
                _status.value = PostApiStatus.DONE
                _properties.value = listResult
            } catch (t: Throwable) {
                _status.value = PostApiStatus.ERROR
                _properties.value = ArrayList()
            }
        }
    }
    private val _navigateToPost = MutableLiveData<Long>()
    val navigateToPost
        get() = _navigateToPost

    fun onPostClick(id: Long){
        _navigateToPost.value = id
    }
    fun onPostNavigated() {
        _navigateToPost.value = null
    }
}
