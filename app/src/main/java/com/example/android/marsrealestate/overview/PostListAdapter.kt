package com.example.android.marsrealestate.overview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.android.marsrealestate.databinding.GridViewItemBinding

import com.example.android.marsrealestate.network.PostProperty


class PostListAdapter(val clickListener: PostListener) : ListAdapter<PostProperty, PostListAdapter.ViewHolder>(PostDiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position)!!, clickListener)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: GridViewItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: PostProperty, clickListener: PostListener) {
            binding.property = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = GridViewItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

}

class PostDiffCallback : DiffUtil.ItemCallback<PostProperty>() {
    override fun areItemsTheSame(oldItem: PostProperty, newItem: PostProperty): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: PostProperty, newItem: PostProperty): Boolean {
        return oldItem.body.equals(newItem.body) && oldItem.title.equals(newItem.title)
    }
}

class PostListener(val clickListener: (postId: Long) -> Unit) {
    fun onClick(post: PostProperty) = clickListener(post.id)
}